﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Trampasjuego : EditorWindow
{
    
     [MenuItem("Window/Pantallatrampas")]
    static void Init() 
    {
        Trampasjuego myWindow = GetWindow<Trampasjuego>();
        myWindow.Show();
        
    }

    void OnGUI()
    {
        Cheats.MutearTodo =
            EditorGUILayout.Toggle("Mute All Sounds", Cheats.MutearTodo);
        Cheats.VidasExtra =
            EditorGUILayout.IntField("Player Lifes", Cheats.VidasExtra);
        Cheats.NombreJugador =
            EditorGUILayout.TextField("Player Two Name", Cheats.NombreJugador);

        GUILayout.FlexibleSpace();

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        GUI.backgroundColor = Color.red;

        if (GUILayout.Button("Reset", GUILayout.Width(100), GUILayout.Height(30)))
        {
            Cheats.MutearTodo = false;
            Cheats.VidasExtra = 4;
            Cheats.NombreJugador = "InsertarNombre";
        }
        EditorGUILayout.EndHorizontal();
    }    
}

