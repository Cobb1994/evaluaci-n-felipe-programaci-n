﻿#if UNITY_EDITOR
using UnityEditor;
#endif

public class Cheats
{
    public static bool MutearTodo
    {
        get
        {
#if UNITY_EDITOR
            return EditorPrefs.GetBool("MutearTodo", false);
#else
            return false;
#endif
        }

        set
        {
#if UNITY_EDITOR
            EditorPrefs.SetBool("MutearTodo", value);
#endif
        }
    }

    public static int VidasExtra
    {
        get
        {
#if UNITY_EDITOR
            return EditorPrefs.GetInt("VidasExtra", 3);
#else
            return false;
#endif
        }

        set
        {
#if UNITY_EDITOR
            EditorPrefs.SetInt("VidasExtra", value);
#endif
        }
    }

    public static string NombreJugador
    {
        get
        {
#if UNITY_EDITOR
            return EditorPrefs.GetString("NombreJugador", "InsertarNombre");
#else
            return false;
#endif
        }

        set
        {
#if UNITY_EDITOR
            EditorPrefs.SetString("NombreJugador", value);
#endif
        }
    }
}
